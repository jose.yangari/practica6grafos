
package lista.controlador;

public class Cola<T> extends Lista<T> {

    private int tamanio;

    public Cola(int tamanio) {
        this.tamanio = tamanio;
    }

    public Cola() {
        this.tamanio = 1;
    }
    
    public boolean queue(T dato){
        if(tamanio() < tamanio){
            return insertarNodos(dato);
        }else{
            return false;
        }
    }     
}


