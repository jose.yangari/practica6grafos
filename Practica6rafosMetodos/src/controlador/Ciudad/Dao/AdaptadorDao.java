/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.Ciudad.Dao;

import controlador.tda.grafo.Adycencia;
import controlador.tda.grafo.GrafoEND;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import lista.controlador.Lista;

/**
 *
 * @author Jose Yangari
 */
public class AdaptadorDao<T> implements InterfazDao<T> {

    private Class<T> clazz;
    private String carpeta = "datos" + File.separatorChar;
    private GrafoEND<T> grafo;

    public AdaptadorDao(Class<T> clazz) {
        this.clazz = clazz;
        carpeta += this.clazz.getSimpleName().toLowerCase() + ".txt";
    }

    public GrafoEND<T> getGrafo() {
        return grafo;
    }

    public void setGrafo(GrafoEND<T> grafo) {
        this.grafo = grafo;
    }

    private void añadirVertice() {
        if (grafo == null) {
            grafo = new GrafoEND<>(1);
        } else {
            GrafoEND aux = new GrafoEND(grafo.numVertices() + 1);
            for (int i = 1; i <= grafo.numVertices(); i++) {
                aux.etiquetarVertice(i, grafo.obtenerEtiqueta(i));
                Lista<Adycencia> lista = grafo.adyacentes(i);
                for (int j = 0; j < lista.tamanio(); j++) {
                   Adycencia ad = lista.consultarDatoPosicion(j);
                   aux.insertarArista(i, ad.getDestino(), ad.getPeso()); 
                }
            }
            grafo = aux;
        }
    }
    
    public Boolean actualizarGrafo(GrafoEND grafoaux){
        //listarGrafo();
        try {
           // listarGrafo();
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(carpeta));
            
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error en actualizar grafo" + e);
        }
        return false;
    }

    @Override
    public boolean guardar(T dato) {
        try {
            //guardar en grafo se debe aumentar la dimension del grafo
            listarGrafo();
            añadirVertice();
            grafo.etiquetarVertice(grafo.numVertices(), dato);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(carpeta));
            
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modificar(T dato) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(carpeta));
            grafo = listarGrafo();
            //aux.modificarPorPos(dato);
            oos.writeObject(grafo);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error al guardar: " + e);
        }
        return false;
    }

    @Override
    public GrafoEND<T> listarGrafo() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(carpeta));
            grafo = (GrafoEND<T>) ois.readObject();
            ois.close();
        } catch (Exception e) {
            System.out.println("Error en listar: " + e);
        }
        return grafo;
    }

}
