/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.Ciudad.Dao;

import Modelo.Ciudad;

/**
 *
 * @author Jose Yangari
 */
public class CiudadDao extends AdaptadorDao<Ciudad> {

    private Ciudad ciudad;

    public CiudadDao() {
        super(Ciudad.class);
        listarGrafo();
    }

    public Ciudad getCiudad() {
        if (ciudad == null) {
            ciudad = new Ciudad();
        }
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Boolean guardar() {
        Integer aux = (getGrafo() != null)? getGrafo().numVertices() + 1 : 1;
        ciudad.setId2(new Long(String.valueOf(aux)));
        return guardar(ciudad);
    }

    public Boolean modificar() {
        return modificar(ciudad);
    }

}
