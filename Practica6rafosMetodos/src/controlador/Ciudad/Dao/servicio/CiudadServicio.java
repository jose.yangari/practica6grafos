/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.Ciudad.Dao.servicio;

import Modelo.Ciudad;
import controlador.Ciudad.Dao.CiudadDao;
import controlador.tda.grafo.GrafoEND;

/**
 *
 * @author Jose Yangari
 */
public class CiudadServicio {
    private CiudadDao ciudadDao = new CiudadDao();

    public Ciudad getCiudad() {
        return ciudadDao.getCiudad();
    }
    
    public void fijarCiudad(Ciudad ciudad){
        ciudadDao.setCiudad(ciudad);
    }
    
    public Boolean guardar(){
        return ciudadDao.guardar();
    }
    
    public GrafoEND<Ciudad> getGrafo(){
        return ciudadDao.listarGrafo();
    }
    
    public GrafoEND<Ciudad> getGrafoObjeto(){
        return ciudadDao.getGrafo();
    }
    
    public boolean actualizarGrafo(GrafoEND grafoaux){
        return ciudadDao.actualizarGrafo(grafoaux);
    }
}
