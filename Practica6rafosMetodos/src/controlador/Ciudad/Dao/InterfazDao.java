/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.Ciudad.Dao;

import controlador.tda.grafo.GrafoEND;

/**
 *
 * @author Jose Yangari
 */
public interface InterfazDao <T> {
    public boolean guardar(T dato);
    public boolean modificar(T dato);
    public GrafoEND<T> listarGrafo();
   // public T buscarId(Long id);
}
