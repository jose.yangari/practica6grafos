/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import java.util.HashMap;
import lista.controlador.Lista;

/**
 *
 * @author Jose Yangari
 */
public class GrafoED<E> extends GrafoD {

    private E etiquetas[];
    private HashMap<E, Integer> dicVertices;

    public GrafoED(Integer numV) {
        super(numV);
        this.etiquetas = (E[]) new Object[numV + 1];
        dicVertices = new HashMap<>(numV);
    }

    public Boolean existeAristaE(E i, E j) {
        try {
            return this.existeArista(obtenerCodigo(i), obtenerCodigo(j));
        } catch (Exception e) {
            return false;
        }
    }

    public Integer obtenerCodigo(E etiqueta) {
        try {
            return dicVertices.get(etiqueta);
        } catch (Exception e) {
            return -1;
        }
    }

    public void insertarAristaE(E i, E j, Double peso) {
        try {
            this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
        } catch (Exception e) {
        }
    }

    public void insertarAristaE(E i, E j) {
        try {
            this.insertarArista(obtenerCodigo(i), obtenerCodigo(j));
        } catch (Exception e) {
        }
    }

    public Lista<Adycencia> adyacentesDE(E i) {
        return adyacentes(obtenerCodigo(i));
    }

    public void etiquetarVertice(Integer codigo, E etiqueta) {
        etiquetas[codigo] = etiqueta;
        dicVertices.put(etiqueta, codigo);
    }

    public E obtenerEtiqueta(Integer codigo) {
        return etiquetas[codigo];
    }

    @Override
    public String toString() {
        String grafo = "";
        for (int i = 1; i <= numVertices(); i++) {
            grafo += "Vertice " + i + "E ( "+obtenerEtiqueta(i).toString()+")";
            Lista<Adycencia> lista = adyacentes(i);
            for (int j = 0; j < lista.tamanio(); j++) {
                Adycencia aux = lista.consultarDatoPosicion(j);
                if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                    grafo += " --Vertice destino " + aux.getDestino() + " etiqueta (" + obtenerEtiqueta(aux.getDestino()).toString() + ")" + " ---SP";
                } else {
                    grafo += " --Vertice destino " + aux.getDestino() + " etiqueta (" + obtenerEtiqueta(aux.getDestino()).toString() + ")" + " --Peso: " + aux.getPeso() + " ---";
                }
            }
            grafo += "\n";
        }
        return grafo;
    }
}
