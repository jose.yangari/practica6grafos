/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import controlador.tda.lista.exepcion.AdyacenciaExepcion;
import java.io.Serializable;
import lista.controlador.Cola;
import lista.controlador.Lista;

/**
 *
 * @author Jose Yangari
 */
public abstract class Grafo<E> implements Serializable {

    protected int visitados[];
    protected int ordenVisita;
    protected Cola<Integer> q;

    //ALGORITMO FLOYD
    private Double[][] matrizAdyacencia;
    private int[][] caminos;
    private int[][] d;

    public void preparar() {
        matrizAdyacencia = new Double[numVertices()][numVertices()];
        caminos = new int[numVertices()][numVertices()];
        d = new int[numVertices()][numVertices()];
    }

    //algortimo 
    public void floyd() {
        for (int i = 0; i < numVertices(); i++) {
            for (int j = 0; j < numVertices(); j++) {
                d[i][j] = (int) Math.round(matrizAdyacencia[i][j]);
                caminos[i][j] = -1;
            }
        }

        for (int i = 0; i < numVertices(); i++) {
            d[i][i] = 0;
            for (int k = 0; k < numVertices(); k++) {
                for (int j = 0; j < numVertices(); j++) {
                    for (int l = 0; l < numVertices(); l++) {
                        if ((d[j][k] + d[k][l]) < d[j][l]) {
                            d[j][l] = d[j][k] + d[k][l];
                            caminos[j][l] = k;
                        }
                    }
                }
            }
        }
        
        System.out.println("CAMINOS");
        for (int i = 0; i < caminos.length; i++) {
            for (int j = 0; j < caminos.length; j++) {
                System.out.print(caminos[i][j] + "\t");
            }
            System.out.println("");

        }
        System.out.println("Matriz D");
        for (int i = 0; i < d.length; i++) {
            for (int j = 0; j < d.length; j++) {
                System.out.print(d[i][j] + "\t");
            }
            System.out.println("");

        }

    }
    //generar matriz e adyacencias;

    public void generarMatriz() {
        for (int i = 0; i < numVertices(); i++) {
            Lista<Adycencia> lista = adyacentes(i + 1);
            for (int j = 0; j < lista.tamanio(); j++) {
                Adycencia ady = lista.consultarDatoPosicion(j);
                matrizAdyacencia[i][ady.getDestino() - 1] = ady.getPeso();
            }
        }

        for (int i = 0; i < numVertices(); i++) {
            for (int j = 0; j < numVertices(); j++) {
                if (matrizAdyacencia[i][j] == null) {
                    matrizAdyacencia[i][j] = -1.0;
                }
            }
        }
    }

    //BUSQUEDA POR PROFUNDIDAD
    public int[] toArrayDFS() {
        int res[] = new int[numVertices() + 1];
        visitados = new int[numVertices() + 1];
        ordenVisita = 1;
        for (int i = 1; i <= numVertices(); i++) {
            if (visitados[i] == 0) {
                toArrayDFS(i, res);
            }
        }
        return res;
    }

    protected void toArrayDFS(int origen, int res[]) {
        res[ordenVisita] = origen;
        visitados[origen] = ordenVisita++;
        Lista<Adycencia> L = adyacentes(origen);
        for (int i = 0; i < L.tamanio(); i++) {
            Adycencia a = L.consultarDatoPosicion(i);
            if (visitados[a.getDestino()] == 0) {
                toArrayDFS(a.getDestino(), res);
            }
        }
    }

    public String toStringDFS() {
        return arrayToString(toArrayDFS());
    }

    protected String arrayToString(int v[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < v.length; i++) {
            sb.append(v[i] + "\t");
        }
        return sb.toString();
    }

    //BUSQUEDA ANCHURA
    public String toStringBFS() {
        return arrayToString(toArrayBFS());
    }

    public int[] toArrayBFS() {
        int res[] = new int[numVertices() + 1];
        visitados = new int[numVertices() + 1];
        ordenVisita = 1;
        q = new Cola<>();
        for (int i = 1; i <= numVertices(); i++) {
            if (visitados[i] == 0) {
                toArrayBFS(i, res);
            }
        }
        return res;
    }

    protected void toArrayBFS(int origen, int res[]) {
        res[ordenVisita] = origen;
        visitados[origen] = ordenVisita++;
        q.queue(origen);
        while (!q.estaVacia()) {
            int u = q.extraer();
            Lista<Adycencia> L = adyacentes(u);
            for (int i = 0; i < L.tamanio(); i++) {
                Adycencia a = L.consultarDatoPosicion(i);
                if (visitados[a.getDestino()] == 0) {
                    res[ordenVisita] = a.getDestino();
                    visitados[a.getDestino()] = ordenVisita++;
                    q.queue(a.getDestino());
                }
            }
        }
    }

    /**
     * Es el numero de vertices del grafo
     *
     * @return Integer numero de vertices
     */
    public abstract Integer numVertices();

    /**
     * Es el numero de aristas del grafo
     *
     * @return Integer numero de aristas
     */
    public abstract Integer numAristas();

    /**
     * Permite ver si entre dos nodos hay conexion (arista)
     *
     * @param i Nodo incial
     * @param j Nodo final
     * @return true si estan conectados
     * @throws Exception Excepcion
     */
    public abstract Boolean existeArista(Integer i, Integer j) throws Exception;

    /**
     * Retorna el peso que hay entre dos vertices
     *
     * @param i Nodo incial
     * @param j Nodo final
     * @return Double peso de la arista
     */
    public abstract Double pesoArista(Integer i, Integer j);

    /**
     * Permite insertar arista sin peso
     *
     * @param i Nodo incial
     * @param j Nodo final
     */
    public abstract void insertarArista(Integer i, Integer j);

    /**
     * Permite insertar arista con peso
     *
     * @param i Nodo incial
     * @param j Nodo final
     * @param peso peso de la arista
     */
    public abstract void insertarArista(Integer i, Integer j, Double peso);

    /**
     * Listado de adycencias de un nodo
     *
     * @param i Nodo a listar sus adyacencias
     * @return Lista
     */
    public abstract Lista<Adycencia> adyacentes(Integer i);

    @Override
    public String toString() {
        String grafo = "";
        for (int i = 1; i <= numVertices(); i++) {
            grafo += "Vertice " + i;
            Lista<Adycencia> lista = adyacentes(i);
            for (int j = 0; j < lista.tamanio(); j++) {
                Adycencia aux = lista.consultarDatoPosicion(j);

                if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                    grafo += " --Vertice destino " + aux.getDestino() + "-- SP ";
                } else {
                    grafo += " --Vertice destino " + aux.getDestino() + "-- Peso " + aux.getPeso() + "--";
                }

            }
            grafo += "\n";
        }
        return grafo;
    }

    public Lista caminiMinimo(Integer verticeI, Integer verticeF) throws AdyacenciaExepcion {
        Lista<Integer> caminos = new Lista();

        Lista<Double> listaPesos = new Lista();
        Integer inicial = verticeI;
        caminos.insertarNodo(inicial);
        Boolean finalizar = false;
        while (!finalizar) {
            Lista<Adycencia> adyacencias = adyacentes(inicial);
            if (adyacencias == null) {
                throw new AdyacenciaExepcion("No existe ningun tipo de adyacencias");
            }
            Double peso = 100000000.0;
            Integer T = -1;
            for (int i = 0; i < adyacencias.tamanio(); i++) {
                Adycencia ad = adyacencias.consultarDatoPosicion(i);

                if (!estaenCamino(caminos, ad.getDestino())) {
                    Double pesoArista = ad.getPeso();
                    if (verticeF.intValue() == ad.getDestino().intValue()) {
                        T = ad.getDestino();
                        peso = pesoArista;
                        break;
                    } else if (pesoArista < peso) {
                        T = ad.getDestino();
                        peso = pesoArista;
                    }
                }
            }
            listaPesos.insertarNodo(peso);
            caminos.insertarNodo(T);
            inicial = T;
            if (verticeF.intValue() == inicial.intValue()) {
                finalizar = true;
            }
        }

        return caminos;
    }

    public Boolean estaenCamino(Lista<Integer> lista, Integer vertice) {
        Boolean band = false;
        for (int i = 0; i < lista.tamanio(); i++) {
            if (lista.consultarDatoPosicion(i).intValue() == vertice.intValue()) {
                band = true;
            }
        }
        return band;
    }
}
