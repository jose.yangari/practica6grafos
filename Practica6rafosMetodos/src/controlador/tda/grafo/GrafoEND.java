/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.grafo;

import java.io.Serializable;

/**
 *
 * @author Jose Yangari
 */
public class GrafoEND <E> extends GrafoED<E> implements Serializable{

    public GrafoEND(Integer numV) {
        super(numV);
    }
    
    @Override
    public void insertarArista(Integer i, Integer j, Double peso) {
        try {
            if (i.intValue() <= numVertices() && j.intValue() <= numVertices()) {
                if (!existeArista(i, j)) {
                    numA++;
                    listaAdycente[i].insertarNodo(new Adycencia(j, peso));
                    listaAdycente[j].insertarNodo(new Adycencia(i, peso));
                }
            }
        } catch (Exception e) {
            System.out.println("Error en insertar GND");
        }
    }

    @Override
    public void insertarAristaE(E i, E j, Double peso) {
        try {
            insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
            insertarArista(obtenerCodigo(j), obtenerCodigo(i), peso);
        } catch (Exception e) {
        }
    }
    
    
    
}
