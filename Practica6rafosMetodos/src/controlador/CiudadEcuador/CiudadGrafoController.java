/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.CiudadEcuador;

import Modelo.Ciudad;
import Modelo.TipoCiudad;
import Modelo.Ubicacion;
import controlador.tda.grafo.GrafoEND;
import controlador.tda.grafo.GrafoEND1;

/**
 *
 * @author Jose Yangari
 */
public class CiudadGrafoController {
    private GrafoEND1<Ciudad> gend;
    private Ciudad ciudad;

    public CiudadGrafoController(Integer nro_vertices) {
        gend = new GrafoEND1<>(nro_vertices, Ciudad.class);
        for(int i = 1; i <= nro_vertices; i++) {
            Ciudad p = new Ciudad();
            p.setId(i);
            p.setNombre("Ciudad "+i);
            p.setTipoCiudad(TipoCiudad.GRANDE);
            Ubicacion u = new Ubicacion();
            u.setId(i);
            u.setLatitud(0.0);
            u.setLongitud(0.0);
            p.setUbicacion(u);
            gend.etiquetarVertice(i, p);
        }
    }

    public GrafoEND1<Ciudad> getGend() {
        return gend;
    }

    public void setGend(GrafoEND1<Ciudad> gend) {
        this.gend = gend;
    }

    public Ciudad getCiudad() {
        if(ciudad == null)
            ciudad = new Ciudad();        
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    
    public Double calcularDistancia(Ciudad po, Ciudad pd) {
        // raiz cuadrara ((x1 - x2)elevado al cuadradro + (y1 - y2)elevado al cuadradro)
        Double dis = 0.0;
        Double x = po.getUbicacion().getLongitud() - pd.getUbicacion().getLongitud();
        Double y = po.getUbicacion().getLatitud() - pd.getUbicacion().getLatitud();
        dis = Math.sqrt((x*x) + (y*y));
        return dis;
    }
    
    
}
