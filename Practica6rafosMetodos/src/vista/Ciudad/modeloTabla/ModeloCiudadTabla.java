/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.Ciudad.modeloTabla;

import controlador.tda.grafo.GrafoEND;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Jose Yangari
 */
public class ModeloCiudadTabla extends AbstractTableModel {

    private GrafoEND grafoEND;
    private String[] columnas;

    public GrafoEND getGrafoND() {
        return grafoEND;
    }

    public void setGrafoD(GrafoEND grafoND) {
        this.grafoEND = grafoND;
        generarColumnas();
    }

    @Override
    public int getRowCount() {
        return grafoEND.numVertices();
    }

    @Override
    public int getColumnCount() {
        return grafoEND.numVertices() + 1;
    }

    private String[] generarColumnas() {
        columnas = new String[grafoEND.numVertices() + 1];
        columnas[0] = "--";
        for (int i = 1; i < columnas.length; i++) {
            columnas[i] = grafoEND.obtenerEtiqueta(i).toString();
        }
        return columnas;
    }

    @Override
    public String getColumnName(int i) {
        return columnas[i]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int i, int i1) {
        if (i1 == 0) {
            return columnas[i + 1];
        } else {
            try {
                if (grafoEND.existeArista((i + 1), i1)) {
                    return grafoEND.pesoArista(i + 1, i1);
                } else {
                    return "--";
                }
            } catch (Exception e) {
                System.out.println("Error en ver arista");
            }
        }
        return "";
    }

}
