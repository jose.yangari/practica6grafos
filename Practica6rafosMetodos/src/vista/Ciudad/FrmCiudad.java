/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.Ciudad;

import controlador.Ciudad.Dao.AdaptadorDao;
import controlador.Ciudad.Dao.servicio.CiudadServicio;
import controlador.CiudadEcuador.CiudadGrafoController;
import controlador.tda.lista.exepcion.AdyacenciaExepcion;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import lista.controlador.Lista;
import vista.Ciudad.modeloTabla.ModeloCiudadTabla;

/**
 *
 * @author Jose Yangari
 */
public class FrmCiudad extends javax.swing.JDialog {

    private CiudadServicio cs = new CiudadServicio();
    private CiudadGrafoController cgc;
    private ModeloCiudadTabla mct = new ModeloCiudadTabla();

    /**
     * Creates new form Frmpozo
     */
    public FrmCiudad(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        limpiarDatos();
    }

    private Boolean validarDatos() {
        return (txtciudad.getText().trim().length() > 0 && txtlatitud.getText().trim().length() > 0
                && txtlongitud.getText().trim().length() > 0 && txtcapital.getText().trim().length() > 0
                && txtclima.getText().trim().length() > 0);
    }

    private void cargarTabla() {
        if (cs.getGrafo() != null) {
            mct.setGrafoD(cs.getGrafo());
            tblciudades.setModel(mct);
            mct.fireTableStructureChanged();
            tblciudades.updateUI();
            System.out.println(mct.getColumnCount() + "---");
        } else {
            DefaultTableModel modelo = new DefaultTableModel(1, 1);
            tblciudades.setModel(modelo);
            tblciudades.updateUI();
            System.out.println(modelo.getColumnCount());
        }
    }

    private void cargarCombos() {
        cbxorigen.removeAllItems();
        cbxdestino.removeAllItems();
        if (cs.getGrafo() != null) {
            for (int i = 0; i < cs.getGrafo().numVertices(); i++) {
                String label = cs.getGrafo().obtenerEtiqueta(i + 1).toString();
                cbxdestino.addItem(label);
                cbxorigen.addItem(label);
            }
        }
    }

    private void limpiarDatos() {
        txtcapital.setText("");
        txtlatitud.setText("");
        txtlongitud.setText("");
        txtciudad.setText("");
        txtclima.setText("");
        cs.fijarCiudad(null);
        cargarTabla();
        cargarCombos();
    }

    private void agregarAdyacencia() {
        if (cs.getGrafo() != null) {
            if (cbxorigen.getSelectedIndex() == cbxdestino.getSelectedIndex()) {
                JOptionPane.showMessageDialog(null, "No se puede agregar un mismo vertice", "ERROR", JOptionPane.ERROR_MESSAGE);
            } else {
                //longitud x
                //latitud y

                Integer p1 = cbxorigen.getSelectedIndex() + 1;
                Integer p2 = cbxdestino.getSelectedIndex() + 1;

                Double xcuadrado = Math.pow(cs.getGrafo().obtenerEtiqueta(p1).getLongitud() - cs.getGrafo().obtenerEtiqueta(p2).getLongitud(), 2);
                Double ycuadrado = Math.pow(cs.getGrafo().obtenerEtiqueta(p1).getLatitud() - cs.getGrafo().obtenerEtiqueta(p2).getLatitud(), 2);
                Double distancia = Math.sqrt(ycuadrado + xcuadrado);
                cs.getGrafo().insertarArista(p1, p2, distancia);
                if (cs.actualizarGrafo(cs.getGrafoObjeto())) {
                    JOptionPane.showMessageDialog(this, "Grafo actualizado", "OK", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "faltan datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                };

                limpiarDatos();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Faltan datos", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void generarCaminos() throws AdyacenciaExepcion {
        Integer origen = cbxorigen.getSelectedIndex();
        Integer destino = cbxdestino.getSelectedIndex();

        if (cs.getGrafo() != null) {

            if (origen == destino) {
                JOptionPane.showMessageDialog(null, "ESCOJA CIUDADES DIFERENTES", "ERROR", JOptionPane.ERROR_MESSAGE);
            } else {
                //longitud x
                //latitud y

                Integer p1 = cbxorigen.getSelectedIndex() + 1;
                Integer p2 = cbxdestino.getSelectedIndex() + 1;

//                Lista<Integer> caminos = cs.getGrafoObjeto().caminiMinimo(p1, p2);
                cs.getGrafoObjeto().preparar();
                cs.getGrafoObjeto().generarMatriz();
                cs.getGrafoObjeto().floyd();
//                cs.getGrafoObjeto().recuperar_camino(p1, p2);
//                try {
//                    String[] aux = new String[caminos.tamanio()];
//                    for (int i = 0; i < caminos.tamanio(); i++) {
//                        aux[i] = cgc.getGend().obtenerEtiqueta(caminos.obtenerDato(i)).toString();
//                    }
//                    jlista.setListData(aux);
//                } catch (Exception e) {
//                }

            }
        }
    }

    private void agregar() {
        if (validarDatos()) {
            try {
                cs.getCiudad().setNombre(txtciudad.getText());
                cs.getCiudad().setCapital(txtcapital.getText());
                cs.getCiudad().setLatitud(Double.parseDouble(txtlatitud.getText()));
                cs.getCiudad().setLongitud(Double.parseDouble(txtlongitud.getText()));
                cs.getCiudad().setClima(txtclima.getText());
                if (cs.guardar()) {
                    JOptionPane.showMessageDialog(this, "Ciudad registrada", "OK", JOptionPane.INFORMATION_MESSAGE);
                    limpiarDatos();

                } else {
                    JOptionPane.showMessageDialog(this, "No se pudo guardar", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Faltan datos", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtciudad = new javax.swing.JTextField();
        txtlongitud = new javax.swing.JTextField();
        txtcapital = new javax.swing.JTextField();
        txtlatitud = new javax.swing.JTextField();
        btnguardar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtclima = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblciudades = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cbxorigen = new javax.swing.JComboBox<>();
        cbxdestino = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        txtcamino = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 204, 204));
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 3, 12))); // NOI18N
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Ciudad");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 30, 60, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Longitud");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(30, 80, 80, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Capital");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(260, 20, 90, 50);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Latitud");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 120, 60, 30);
        jPanel1.add(txtciudad);
        txtciudad.setBounds(90, 30, 140, 30);
        jPanel1.add(txtlongitud);
        txtlongitud.setBounds(90, 80, 140, 30);
        jPanel1.add(txtcapital);
        txtcapital.setBounds(320, 30, 140, 30);
        jPanel1.add(txtlatitud);
        txtlatitud.setBounds(90, 120, 140, 30);

        btnguardar.setBackground(new java.awt.Color(204, 204, 255));
        btnguardar.setText("Guardar");
        btnguardar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnguardar);
        btnguardar.setBounds(720, 130, 80, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Clima");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(490, 30, 50, 30);
        jPanel1.add(txtclima);
        txtclima.setBounds(540, 30, 140, 30);

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Datos de Ciudad", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 10, 690, 160);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 830, 190);

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Tabla de Uniones de las Ciudades", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("BankGothic Lt BT", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jPanel2MouseMoved(evt);
            }
        });
        jPanel2.setLayout(null);

        tblciudades.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tblciudades.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblciudades);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(20, 100, 540, 140);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Origen");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(30, 40, 60, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Destino");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(210, 40, 60, 30);

        cbxorigen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxorigen.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.add(cbxorigen);
        cbxorigen.setBounds(80, 40, 100, 30);

        cbxdestino.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxdestino.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.add(cbxdestino);
        cbxdestino.setBounds(280, 40, 100, 30);

        jButton1.setText("VINCULAR");
        jButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(410, 40, 160, 30);

        jButton3.setText("CAMINO");
        jButton3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(580, 190, 160, 30);
        jPanel2.add(txtcamino);
        txtcamino.setBounds(580, 130, 160, 30);

        jButton2.setText("MOSTRAR");
        jButton2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(610, 40, 70, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(20, 200, 790, 270);

        setSize(new java.awt.Dimension(840, 526));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
        // TODO add your handling code here:
        agregar();
    }//GEN-LAST:event_btnguardarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        new FrmPresentarCiudadesGrafo(null, true, cs).setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        agregarAdyacencia();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            // TODO add your handling code here:
            generarCaminos();
        } catch (AdyacenciaExepcion ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jPanel2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseMoved
        // TODO add your handling code here:

    }//GEN-LAST:event_jPanel2MouseMoved

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmCiudad dialog = new FrmCiudad(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnguardar;
    private javax.swing.JComboBox<String> cbxdestino;
    private javax.swing.JComboBox<String> cbxorigen;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tblciudades;
    private javax.swing.JTextField txtcamino;
    private javax.swing.JTextField txtcapital;
    private javax.swing.JTextField txtciudad;
    private javax.swing.JTextField txtclima;
    private javax.swing.JTextField txtlatitud;
    private javax.swing.JTextField txtlongitud;
    // End of variables declaration//GEN-END:variables
}
